import { IWallComponent } from "./IWallComponent";

export class Window implements IWallComponent {
    private id: number;
    private height: number;
    private width: number;
    private unit: string;

    constructor (id = 0, height = 1.2, width = 2.0) {
        this.id = id;
        this.height = height;
        this.width = width;
        this.unit = "m";
    }

    getId () {
        return this.id;
    }

    getHeight () {
        return this.height;
    }

    getWidth () {
        return this.width;
    }

    getUnit () {
        return this.unit;
    }

    getArea () {
        return this.height * this.width;
    }
}