import { Door } from "./Door";
import { IWallComponent } from "./IWallComponent";

export class Wall {
    private id: number;
    private height: number;
    private width: number;
    private unit = "m";
    private wallComponents: IWallComponent[];

    constructor(id = 0, height = 1, width = 1) {
        this.id = id;
        if (this.isAreaSizeOk(height * width)) {
            this.height = height;
            this.width = width;
        } else {
            throw new Error("Area size must be between 1 and 15");
        }
        this.wallComponents = [];
    }

    getId(): number {
        return this.id;
    }

    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }

    getArea() {
        return this.height * this.width;
    }

    getUnit() {
        return this.unit;
    }

    public getWallComponents(): IWallComponent[] {
        return this.wallComponents;
    }

    private getUsedArea(): number{
        /**
         * Returns the sum of the area consumed by
         * the wall components
        */
        return this.wallComponents
            .reduce((acc, component) => acc + component.getArea(), 0);
    }

    private updateHeight(height: number): void {
        /**
         * Checks to see if there's a door in the wall
         * and if there is, checks to see if the new height is
         * greater than the door height plus 30cm
        */
       if (this.hasDoorComponent()) {
           if (!(height >= (this.getFirstDoorHeight() + 0.3))) {
               throw new Error("Height must be greater than \
               the door height plus 30cm");
            }
            this.height = height;
       }
    }
    
    private updateWidth(width: number): void {
        this.width = width;
    }

    private isAreaSizeOk(area: number): boolean {
        return area >= 1 && area <= 15;
    }

    private isAreaUsageOk(area: number): boolean {
        /**
         * Checks to see if the sum of the area consumed by
         * the wall components are equal or less than 50% of the
         * wall area
        */
        return this.getUsedArea() <= (area * 0.5);
    }

    private hasDoorComponent(): boolean {
        return this.wallComponents.some(component => component instanceof Door);
    }

    private getFirstDoorHeight(): number {
        return this.wallComponents
                .filter(component => component instanceof Door)[0]
                .getHeight();
    }

    updateDimensions(height: number, width: number): Error | void {
        if (!this.isAreaSizeOk(height * width)) {
            return new Error("Area size must be between 1 and 15");
        }
        if (!this.isAreaUsageOk(height * width)) {
            return new Error("Wall components can't occupy more \
            than 50% of the wall area");
        }
        this.updateHeight(height);
        this.updateWidth(width);
    }

    addWallComponent(wallComponent: IWallComponent): void {
        this.wallComponents.push(wallComponent);
    }

    removeWallComponent(componentId: number): void {
        this.wallComponents.splice(
            this.wallComponents.findIndex(
                component => component.getId() === componentId
            ), 1
        );
    }

    public getAvailableArea(): number{
        return this.getArea() - this.getUsedArea();
    }
}