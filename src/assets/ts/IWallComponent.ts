export interface IWallComponent {
    getId(): number;
    getHeight(): number;
    getWidth(): number;
    getArea(): number;
    getUnit(): string;
}