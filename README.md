# Digital Republic Code Challenge

### Compatibility Note
<mark>Vite requires Node.js version >=12.2.0. However, some templates
require a higher Node.js version to work, please upgrade if your
package manager warns about it.</mark>

---

## What Am I Using?
- Vue 3
- Vite
- TypeScript

The app is just a **Single Page Application**.

## What I'm Not Using
- A web server
## Install the dependencies
I encourage you to install the dependencies and use the project in dev mode (instead of building it), because when we build the project, the bundler changes the name of the custom classes written in `assets/ts/`, and because of this
the name of the component that is added to each wall will look awkward.
```
yarn install
```

## Run in dev mode
If you want to make changes to the source code and see the results
happening instantly with hot-reloading, use the command below:
```
yarn dev
```
Follow the instructions in your terminal to open the project
in your browser.
## Build
The command bellow will build the project inside `dist/`
directory.
```
yarn build
```

## Preview
This command will startup a server and serve the built files.
```
yarn preview
```